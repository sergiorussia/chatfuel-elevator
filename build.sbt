name := "chatfuel-elevator"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0" // or "commons-cli" % "commons-cli" % "1.4"

disablePlugins(
  plugins.Giter8TemplatePlugin, // not used
  plugins.JUnitXmlReportPlugin, // we don't need xml reporting
)

assemblyJarName in assembly := "elevator.jar"
