# chatfuel-elevator

https://paper.dropbox.com/doc/Backend-Engineer-gwGdGRZyTyM31UVa3b0fE

## details

- written in Scala
- uses scopt library for args parsing
- using SBT for building

PS: probably Scala is a bit overkill for such simple program which may be rewritten as a single Java class, but who cares, this is fun! 

## howto

to build and check artifact jar:

    make build

to run quick test:

    make test
