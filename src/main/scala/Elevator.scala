import scala.concurrent.{ExecutionContext, Future}
import scala.io.StdIn
import java.util.concurrent.TimeUnit

/**
  * This program assumes that there are many people using elevator, * anybody can call elevator at any time to any floor.
  * With this in mind there is no difference if somebody called elevator to some floor from the outside or told it to go to some floor from inside.
  */
object Elevator extends App {
  // app config

  case class Config(
     floors: Int = 5,
     floorHeight: Int = 3,
     speedElevator: Int = 1,
     speedDoors: Int = 2,
  )

  val argsParser = new scopt.OptionParser[Config]("java -jar elevator.jar") {
    override def showUsageOnError = true
    head("chatfuel elevator by sergiorussia")
    help("help").text("prints this usage text")

    opt[Int]('f', "floors")
      .text("floors, must be within [5, 20]")
      .action((value, config) ⇒ config.copy(floors = value))
      .validate(value ⇒ if (value >= 5 && value <= 20) success else failure("must be within [5, 20]"))

    opt[Int]('h', "height")
      .text("floor height in meters, must be within [1, 10]")
      .action((value, config) ⇒ config.copy(floorHeight = value))
      .validate(value ⇒ if (value >= 1 && value <= 10) success else failure("must be within [1, 10]"))

    opt[Int]('s', "speed")
      .text("elevator speed in meters per second, must be within [1, inf)")
      .action((value, config) ⇒ config.copy(speedElevator = value))
      .validate(value ⇒ if (value >= 1) success else failure("must be within [1, inf)"))

    opt[Int]('d', "dtime")
      .text("doors open/close time in seconds, must be within [1, 60]")
      .action((value, config) ⇒ config.copy(speedDoors = value))
      .validate(value ⇒ if (value >= 1 && value <= 60) success else failure("must be within [1, 60]"))
  }

  argsParser.parse(args, Config()) match {
    case Some(config) ⇒
      val elevator = new Elevator(config, new ElevatorCommandsNonDuplicating, statusUpdater = updateStatus)
      useElevator(config, elevator, userInput = StdIn.readLine, statusUpdater = updateStatus)
    case None ⇒
      sys.exit(1)
  }

  // display

  def updateStatus(message: String): Unit = {
    // we can use some nice library like ncurses here
    // but i'll keep it simple stupid for now
    println(message)
  }

  // elevator

  /** All commands to the elevator are queued. Designed to be used as multiple producers, single consumer */
  trait ElevatorCommands {
    /** Add command, same semantics as for [[java.util.concurrent.BlockingQueue]] */
    def add(floor: Int): Boolean

    /** Blocking operation, same semantics as for [[java.util.concurrent.BlockingQueue]] */
    @throws[InterruptedException]
    def take(): Int
  }

  /** This impl is useful for debugging */
  class ElevatorCommandsSimple extends ElevatorCommands {
    private val floorsToGo = new java.util.concurrent.LinkedBlockingQueue[Int]
    override def add(floor: Int): Boolean = floorsToGo.add(floor)
    @throws[InterruptedException] override def take(): Int = floorsToGo.take()
  }

  /** This impl handles cases when somebody tries to spam elevator with the same requests */
  class ElevatorCommandsNonDuplicating extends ElevatorCommands {
    private val floorsToGo = collection.mutable.LinkedHashSet.empty[Int]
    private val guard = new Object

    override def add(floor: Int): Boolean = guard.synchronized {
      val added = floorsToGo.add(floor)
      guard.notifyAll()
      added
    }

    @throws[InterruptedException]
    override def take(): Int = guard.synchronized {
      while (floorsToGo.isEmpty) {
        guard.wait()
      } // loop to handle spurious wakeups
      val floor = floorsToGo.head
      floorsToGo.remove(floor)
      floor
    }
  }

  class Elevator(config: Config, commands: ElevatorCommands, statusUpdater: String ⇒ Unit) {
    @inline private def floorHeight = config.floorHeight
    private val positionMin = floorHeight // "hidden" zero floor to simplify math
    private val positionMax = config.floors * floorHeight

    def goToFloor(floor: Int): Unit = {
      val positionToGo = floor * floorHeight
      if (positionToGo < positionMin || positionToGo > positionMax) {
        statusUpdater(s"! ooops, no such floor, valid range is [1, ${config.floors}]")
      } else if (commands.add(floor)) {
        statusUpdater(s"> asked elevator to go to the floor [$floor]")
      } else {
        statusUpdater(s"> you have already asked elevator to go to the floor [$floor], be patient")
      }
    }

    def run()(implicit ec: ExecutionContext): Future[_] = Future {
      var position = positionMin
      statusUpdater(s". elevator is at floor [1], waiting for commands")
      while (true) {
        val floorToGo = commands.take()
        val positionToGo = floorToGo * floorHeight
        statusUpdater(s". elevator was asked to go to the floor [$floorToGo]")
        val direction = positionToGo.compare(position) // > 0 => go up
        while (position != positionToGo) {
          if (direction > 0) {
            position = (position + config.speedElevator) min positionToGo // go up
          } else if (direction < 0) {
            position = (position - config.speedElevator) max positionToGo // go down
          }

          if (position != positionToGo) {
            TimeUnit.SECONDS.sleep(1L)
          }

          statusUpdater(s". elevator is at floor [${position / floorHeight}]")
        }
        statusUpdater(s". opening doors...")
        TimeUnit.SECONDS.sleep(config.speedDoors)
        statusUpdater("  doors are opened, closing them back...")
        TimeUnit.SECONDS.sleep(config.speedDoors)
        statusUpdater("  doors are closed")
      }
    }
  }

  // user interaction

  def useElevator(config: Config, elevator: Elevator, userInput: () ⇒ String, statusUpdater: String ⇒ Unit): Unit = {
    def help(): Unit = statusUpdater(s"""
      |> known commands:
      |NNN - ask elevator to go to NNN floor
      |help - this help
      |exit - exit
      |""".stripMargin)

    statusUpdater(s"""
      |> elevator config:
      |floors - ${config.floors}
      |floor height - ${config.floorHeight}m
      |speed - ${config.speedElevator}m/sec
      |doors time - ${config.speedDoors}sec
      |
      |> simply type commands at any moment to control elevator""".stripMargin)
    help()
    elevator.run()(concurrent.ExecutionContext.global) // fire-n-forget

    val floor = "([1-9]\\d*)".r
    while (true) {
      userInput() match {
        case "exit" ⇒ return // should shutdown everything gracefully
        case floor(f) ⇒ elevator.goToFloor(f.toInt)
        case unknown if unknown.nonEmpty ⇒ help()
        case _ ⇒ // nothing to do
      }
    }
  }
}
