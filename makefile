SHELL := /bin/bash
CURRENT_DIR := $(shell pwd)

all: build

build:
	sbt assembly
	java -jar $(CURRENT_DIR)/target/scala-2.12/elevator.jar --help

test:
	sbt "run -f 10 -h 3 -s 2 -d 3"

clean:
	git clean -xdf -e .idea -e *.iml -e *.ipr -e *.iws
